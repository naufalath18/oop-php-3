<?php
require_once 'Frog.php';
require_once 'Ape.php';


$sheep = new Animal("shaun");
echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false


$sungokong = new Ape("kera sakti");
echo "<br>";
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "<br>";
$kodok->jump() ; // "hop hop"
?>